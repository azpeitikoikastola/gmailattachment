from __future__ import print_function
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from apiclient import errors
from datetime import date
import os
import base64
import json

with open('config.json') as json_data_file:
    data = json.load(json_data_file)

# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/gmail.modify'


def list_labels(service, user_id):
    results = service.users().labels().list(userId=user_id).execute()
    labels = results.get('labels', [])

    if not labels:
        print('No labels found.')
    else:
        print('Labels:')
        for label in labels:
            print(label['name'])
    return labels


def list_messages_with_labels(service, user_id, label_ids=None, q=''):
    """List all Messages of the user's mailbox with label_ids applied.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      label_ids: Only return Messages with these labelIds applied.
      q: gmail search options

    Returns:
      List of Messages that have all required Labels applied. Note that the
      returned list contains Message IDs, you must use get with the
      appropriate id to get the details of a Message.
    """
    try:
        if not label_ids:
            label_ids = []
        response = service.users().messages().list(userId=user_id,
                                                   labelIds=label_ids, q=q).execute()
        messages = []
        if 'messages' in response:
            messages.extend(response['messages'])

        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = service.users().messages().list(userId=user_id,
                                                       labelIds=label_ids,
                                                       pageToken=page_token).execute()
            messages.extend(response['messages'])

        return messages
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def get_date(milis):
    m_time = int(milis)
    if not m_time:
        today = date.today()
    else:
        today = date.fromtimestamp(m_time/1000.)
    return today


def get_school_year(message):
    today = get_date(message['internalDate'])
    if today.month > 9:
        return str(today.year) + '-' + str(today.year + 1)
    else:
        return str(today.year - 1) + '-' + str(today.year)


def folder_path(path, message):
    path += '/' if path[-1] != '/' else ''
    school_year = get_school_year(message) + '/'
    if not os.path.exists(path + school_year):
        os.makedirs(path + school_year)
    return path + school_year


def get_message(service, user_id, msg_id):
    """Get a Message with given ID.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The ID of the Message required.

    Returns:
      A Message.
    """
    try:
        message = service.users().messages().get(userId=user_id, id=msg_id).execute()

        print('Message snippet: %s' % message['snippet'])

        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def attachments2file(service, user_id, msg_id, store_dir, prefix='', suffix=''):
    """Get and store attachment from Message with given id.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: ID of Message containing attachment.
      store_dir: The directory used to store attachments.
      prefix: The prefix of the created file
      suffix: The suffix of the created file
    """
    try:
        message = service.users().messages().get(userId=user_id, id=msg_id).execute()

        for part in message['payload']['parts']:
            if part['filename']:
                if part['body'].get('data'):
                    att_data = part['body']['data']
                else:
                    att_id = part['body']['attachmentId']
                    att = service.users().messages().attachments().get(
                        userId=user_id, messageId=msg_id, id=att_id).execute()
                    att_data = att['data']
                file_data = base64.urlsafe_b64decode(att_data.encode('UTF-8'))
                path = ''.join([store_dir, prefix, part['filename'], suffix])

                f = open(path, 'wb')
                f.write(file_data)
                f.close()

    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def messages2file(service, messages):
    success_messages = []
    for message_id in messages:
        try:
            message = get_message(service, 'me', message_id['id'])
            folder = folder_path(data['destination_folder'], message)
            today = date.strftime(get_date(message['internalDate']), "%Y-%m-%d")
            attachments2file(service, 'me', message_id['id'], folder,
                             prefix=today, suffix='')
            success_messages.append(message_id['id'])
        except:
            pass
    return success_messages


def modify_message(service, user_id, msg_id, msg_labels):
    """Modify the Labels on the given Message.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The id of the message required.
      msg_labels: The change in labels.

    Returns:
      Modified message, containing updated labelIds, id and threadId.
    """
    try:
        message = service.users().messages().modify(userId=user_id, id=msg_id,
                                                    body=msg_labels).execute()

        label_ids = message['labelIds']

        print('Message ID: %s - With Label IDs %s' % (msg_id, label_ids))
        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def archive_messages(service, messages, archive_label):
    label_ids = list(map(lambda x: x['id'], archive_label))
    for message_id in messages:
        modify_labels = {
            "addLabelIds": label_ids,
            "removeLabelIds": []
        }
        modify_message(service, 'me', message_id, modify_labels)


def main():
    """Shows basic usage of the Gmail API.
    Lists the user's Gmail labels.
    """
    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('gmail', 'v1', http=creds.authorize(Http()))

    # Call the Gmail API
    labels = list_labels(service, 'me')
    data_labels = list(filter(lambda x: x['name'] in data['labels'], labels))
    archive_label = list(filter(lambda x: x['name'] == data['archive_label'], labels))
    for i in data_labels:
        results = list_messages_with_labels(service, 'me', label_ids=[i['id']], q="-label:" + data['archive_label'])
        if results:
            result = messages2file(service, results)
            archive_messages(service, result, archive_label)


if __name__ == '__main__':
    main()
